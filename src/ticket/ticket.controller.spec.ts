import { Test, TestingModule } from '@nestjs/testing';
import { TicketController } from './ticket.controller';

describe('TicketController', () => {
  let controller: TicketController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TicketController],
    }).compile();

    controller = module.get<TicketController>(TicketController);
  });

  it('equality', () => {
    expect(2).toEqual(2);
  });
});
