import { Test, TestingModule } from '@nestjs/testing';
import { TicketService } from './ticket.service';

describe('TicketService', () => {
  let service: TicketService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TicketService],
    }).compile();

    service = module.get<TicketService>(TicketService);
  });

  it('New User into the DB', async () => {
    const inp = {
      "name":"manoj",
       "gender":"male",
       "email":"manoj@gmail.com",
      "phone":9578325141,
      "from":"CBE",
      "to":"Chennai",
      "seatnumber":20
      }   

      const input=async () =>{
       await service.newUser(inp);
      }
      expect(inp).toBe(input);
  });


  it('Ticket Status', async () => {

    const inp1=10;

      const input1=async () =>{
       await service.getticketdetails(inp1);
      }
      const inp="close";
      expect(inp).toBe(input1);
  });

  it("Open Tickets in DB",async () => {

    expect(async () => {
      await service.Ticketstatusopen()
    })
    
  });


    
  it("Closed Tickets in DB",async () => {

    expect(async () => {
      await service.Ticketstatusclose()
    })
    
});

it("Passenger Details",async () => {
const input=10;
  expect(async () => {
    await service.passengerdetail(input);
  })
  
});


it("Admin",async () => {
  const username="admin";
  const password="admin";

  const ticket ={
    "seatnumber":10,
    "isbooked":true
  }

    expect(async () => {
      await service.admin(ticket);
    })
    
  });






  it("Get all the users from DB",async () => {

    expect(async () => {
      await service.userlogin();
    })
    
});





  it("User Authentication",async () => {
   
    const Login ={
      "name":"manoj",
      "email":"manoj@gmail.com",
      "password":"manoj"
    }
  
      expect(async () => {
        await service.newLogin(Login);
      })
      
    });



});
 

