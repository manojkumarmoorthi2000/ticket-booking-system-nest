import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { UserDTO } from './dto/user.dto';
import { Ticket } from './interfaces/ticket.interface';
import { TicketDTO } from './dto/ticket.dto';
export declare class TicketService {
    private readonly ticketModel;
    private readonly userModel;
    constructor(ticketModel: Model<Ticket>, userModel: Model<User>);
    newUser(userDTO: UserDTO): Promise<User>;
    Ticketstatusopen(): Promise<Ticket[]>;
    Ticketstatusclose(): Promise<Ticket[]>;
    admin(ticketDTO: TicketDTO): Promise<Ticket>;
    getticketdetails(ID: number): Promise<Ticket>;
    passengerdetail(ID: number): Promise<User[]>;
    updateTicketStatus(ticketID: string, ticketDTO: TicketDTO): Promise<Ticket>;
}
